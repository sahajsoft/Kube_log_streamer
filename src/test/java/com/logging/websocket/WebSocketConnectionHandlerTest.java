package com.logging.websocket;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.socket.WebSocketSession;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketConnectionHandlerTest {

    @InjectMocks
    WebSocketConnectionHandler webSocketConnectionHandler;

    @Mock
    private WebSocketSessionManager webSocketSessionManager;

    @Mock
    WebSocketSession webSocketSession;


    @Test
    public void afterConnectionEstablished() {
        int sessionSize = webSocketConnectionHandler.getSessionSize();

        webSocketConnectionHandler.afterConnectionEstablished(webSocketSession);
        Assert.assertEquals(sessionSize +1 , webSocketConnectionHandler.getSessionSize());
    }

    @Test
    public void shouldIgnoreNullSessions() {
        int sessionSize = webSocketConnectionHandler.getSessionSize();

        webSocketConnectionHandler.afterConnectionEstablished(null);
        Assert.assertEquals(sessionSize, webSocketConnectionHandler.getSessionSize());
    }

//    @Test
//    public void handleTextMessage() throws ApiException, ExecutionException, InterruptedException, IOException {
//        ClusterData clusterData = new ClusterData();
//        clusterData.setPod("testpod");
//        clusterData.setNamespace("namespace");
//        Gson gson = new Gson();
//        String jsonData = gson.toJson(clusterData);
//        ObjectMapper objectMapper = new ObjectMapper();
//        TextMessage textMessage = new TextMessage(jsonData);
//        Mockito.doNothing().when(webSocketSessionManager).handleMessage(clusterData,webSocketSession);
//
//        webSocketConnectionHandler.handleTextMessage(webSocketSession, textMessage);
//        Mockito.verify(webSocketSessionManager,times(1)).handleMessage(clusterData,webSocketSession);
//    }

//    @Test @Ignore
//    public void afterConnectionClosed() {
//        webSocketConnectionHandler.afterConnectionEstablished(webSocketSession);
//        CloseStatus closeStatus = Mockito.mock(CloseStatus.class);
//        int sessionSize = webSocketConnectionHandler.getSessionSize();
//        Assert.assertTrue(sessionSize > 0);
//        webSocketConnectionHandler.afterConnectionClosed(webSocketSession,closeStatus);
//    }


}
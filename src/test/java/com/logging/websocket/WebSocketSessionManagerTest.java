package com.logging.websocket;

import com.logging.CallbackHandler;
import com.logging.connector.KubeApi;
import com.logging.connector.KubeLogConnector;
import com.logging.connector.KubeLogMessageHandler;
import com.logging.model.ClusterData;
import io.kubernetes.client.ApiException;
import com.logging.model.PodNamespace;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketSessionManagerTest {

    private static final String TEST_POD = "testPod";

    private static final List<String> podList = Arrays.asList("pod1", "pods");

    @Mock
    private static ClusterData clusterData;

    @Mock
    private KubeLogMessageHandler podSessionHandler;

    @Mock
    WebSocketSession webSocketSession;

    @Mock
    KubeLogConnector kubeLogConnector;

    @Mock
    KubeApi kubeApi;

    @Mock
    Future future;

    @InjectMocks
    WebSocketSessionManager webSocketSessionManager;

    @Test
    public void scheduleLogFetcherJob() throws ApiException, ExecutionException, InterruptedException, IOException {
        when(clusterData.getPod()).thenReturn(TEST_POD);
        when(kubeApi.getPodsByNamespaceLabel(any(ClusterData.class))).thenReturn(podList);
        doNothing().when(kubeLogConnector).getKubeLogStream(any(PodNamespace.class),any(CallbackHandler.class));

        Future future = webSocketSessionManager.handleMessage(clusterData, webSocketSession);
        Thread.sleep(15);


        assertEquals(1,webSocketSessionManager.getPodsForSession(webSocketSession).size());
    }

//    @Test @Ignore
//    public void shouldNotCreateNewSessionForExistingPod() throws ApiException, ExecutionException, InterruptedException, IOException {
//
//
//        addSession();
//        int numSession = webSocketSessionManager.getNumSessionsForPod(TEST_POD);
//        assertTrue(numSession > 0);
//
//        WebSocketSession webSocketSessionMock = Mockito.mock(WebSocketSession.class);
//        when(clusterData.getPod()).thenReturn(TEST_POD);
//
//        webSocketSessionManager.handleMessage(clusterData, webSocketSessionMock);
//
//        assertEquals(numSession + 1, webSocketSessionManager.getNumSessionsForPod(TEST_POD));
//        assertEquals(TEST_POD, webSocketSessionManager.getPodsForSession(webSocketSessionMock));
//    }
//
//    private void addSession() throws ApiException, ExecutionException, InterruptedException, IOException {
//        when(clusterData.getPod()).thenReturn(TEST_POD);
//        webSocketSessionManager.handleMessage(clusterData, webSocketSession);
//    }
//
//    @Test
//    public void shouldFailTocancelLogFetcherJob() throws ApiException, ExecutionException, InterruptedException, IOException {
//        addSession();
//        assertEquals(TEST_POD, webSocketSessionManager.getPodsForSession(webSocketSession));
//
//        webSocketSessionManager.cancelLogFetcherJob(webSocketSession);
//
//        assertNull(webSocketSessionManager.getPodsForSession(webSocketSession));
//    }
//
//    @Test
//    public void handleMessageShouldReturnFutur() throws InterruptedException, ExecutionException, ApiException, IOException {
//        ThreadPoolExecutor threadPoolExecutor = Mockito.mock(ThreadPoolExecutor.class);
//        Mockito.when(threadPoolExecutor.submit(Mockito.argThat(new ArgumentMatcher<Callable>() {
//            @Override
//            public boolean matches(Callable callable) {
//                try {
//                    callable.call();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return false;
//                }
//                return true;
//            }
//        }))).thenReturn(Mockito.mock(Future.class));
//        Mockito.when(threadPoolExecutor.submit(Mockito.argThat(new ArgumentMatcher<Runnable>() {
//            @Override
//            public boolean matches(Runnable runnable) {
//                try {
//                    runnable.run();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return false;
//                }
//                return true;
//            }
//        }))).thenReturn(Mockito.mock(Future.class));
//        List<String> podList = Arrays.asList("pod1", "pods");
//        when(kubeApi.getPodsByNamespaceLabel(any(ClusterData.class))).thenReturn(podList);
//        webSocketSessionManager.handleMessage(clusterData, webSocketSession);
//
//    }
}
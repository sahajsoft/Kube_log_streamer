package com.logging.connector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class KubeLogMessageHandlerTest {

    private static final String TEST_MESSAGE = "test-message";

    @InjectMocks
    KubeLogMessageHandler podSessionHandler;

    @Mock
    WebSocketSession webSocketSession;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPodName() {
    }

    @Test
    public void setPodName() {
    }

    @Test
    public void getNamespace() {
    }

    @Test
    public void setNamespace() {
    }

    @Test
    public void getLabel() {
    }

    @Test
    public void setLabel() {
    }

    @Test
    public void getWebSocketSessions() {
    }

    @Test
    public void setWebSocketSessions() {
    }

    @Test
    public void setScheduledFuture() {
    }

    @Test
    public void getScheduledFuture() {
    }

    @Test
    public void shouldhandleEmptyMessage() {
        podSessionHandler.onMessage(null);
    }

    @Test
    public void shouldHandleMessage() {
        podSessionHandler.getWebSocketSessions().add(webSocketSession);
        try {
            doNothing().when(webSocketSession).sendMessage(any(TextMessage.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        podSessionHandler.onMessage(TEST_MESSAGE);
//        Assert.assertNotNull(podSessionHandler.getSendLogsToWsSessionsTask());
    }



    @Test
    public void onMessageShouldHandleIoException(){
        podSessionHandler.getWebSocketSessions().add(webSocketSession);
        try {
            doThrow(IOException.class).when(webSocketSession).sendMessage(any(TextMessage.class));
        } catch (IOException e) {
        }

        podSessionHandler.onMessage(TEST_MESSAGE);
    }

    @Test
    public void shouldCancelSendLogsToWSSessionTask(){
        podSessionHandler.getWebSocketSessions().add(webSocketSession);
        try {
            doThrow(IOException.class).when(webSocketSession).sendMessage(any(TextMessage.class));
        } catch (IOException e) {
        }

        podSessionHandler.onMessage(TEST_MESSAGE);
//        Assert.assertTrue(podSessionHandler.cancelSendLogsToWsSessionsTask());

    }

    @Test
    public void shouldNotCancelSendLogsToWSSessionTask(){
//        Assert.assertFalse(podSessionHandler.cancelSendLogsToWsSessionsTask());

    }
}
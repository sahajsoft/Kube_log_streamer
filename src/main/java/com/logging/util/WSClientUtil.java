package com.logging.util;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.*;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import static java.lang.System.exit;

public class WSClientUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(WSClientUtil.class);
    private static CountDownLatch countDownLatch = new CountDownLatch(1);
    private static Options options = new Options();

    public WSClientUtil() {
        initOptions();
    }

    private static void countDown() {
        countDownLatch.countDown();
    }

    private static void printUsageAndExit() {
        HelpFormatter formatter = new HelpFormatter();
        initOptions();
        formatter.printHelp("wsclient", options, true);

        exit(1);
    }

    private static void initOptions() {
        options.addOption("u", "url", true, "WebSocket Server Url");
        options.addOption("h", "help", false, "Show help");
        options.addOption("d", "data", true, "Request Message");
    }

    private static CommandLine parseCommandLine(String[] args) {
        if (args.length < 2) {
            printUsageAndExit();
        }

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException | NumberFormatException e) {
            printUsageAndExit();
        }
        return cmd;
    }

    public static void main(String[] args) throws InterruptedException {
        initOptions();
        CommandLine commandLine = parseCommandLine(args);

        String wsHost = commandLine.getOptionValue('u');

        String data = commandLine.getOptionValue('d');


        WebSocketClient webSocketClient = new StandardWebSocketClient();
        try {
            WebSocketHandler webSocketHandler = getWebSocketHandler(data);
            webSocketClient.doHandshake(webSocketHandler, wsHost);

        } catch (IOException e) {
            LOGGER.error("IO Exception", e);
            exit(1);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOGGER.info("Shutdown");
            countDown();
        }));
        countDownLatch.await();


    }

    private static WebSocketHandler getWebSocketHandler(String data) throws IOException {
        return new WebSocketHandler() {
            @Override
            public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
                webSocketSession.sendMessage(new TextMessage(data));
            }

            @Override
            public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {

                LOGGER.info(webSocketMessage.getPayload().toString());
            }

            @Override
            public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {

            }

            @Override
            public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
                countDown();
            }

            @Override
            public boolean supportsPartialMessages() {
                return false;
            }
        };
    }
}

package com.logging.websocket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.ApiException;
import com.logging.model.ClusterData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class WebSocketConnectionHandler extends TextWebSocketHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketConnectionHandler.class);

    private static final List<WebSocketSession> sessions = new ArrayList<>();

    private static final Object lock = new Object();

    @Autowired
    private WebSocketSessionManager webSocketSessionManager;


    public WebSocketConnectionHandler() {
        super();
    }

    @Override
    public  void afterConnectionEstablished(WebSocketSession session) {
        if (session != null) {

            LOGGER.debug("WS Session {} established", session.toString());
            synchronized (lock) {
                sessions.add(session);
            }
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        ClusterData clusterData = validateAndGetRequestData(session, message);

        if (clusterData != null) {
            LOGGER.debug("Received text message {} from client {} ",message.toString(),session.toString());

            try {
                webSocketSessionManager.handleMessage(clusterData,session);
            } catch (ExecutionException e) {
                LOGGER.error("Execution exception while handling request message",e);
                handleException(e,session);
            } catch (InterruptedException e) {
                LOGGER.debug("Interrupted exception while handling request message",e);
                handleException(e,session);
            }
            catch(ApiException e) {
                LOGGER.error("Error while handling request message",e);
                handleException(e,session);
            }
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        LOGGER.debug("Received connection close request from ws client {} ",session.getRemoteAddress());
        webSocketSessionManager.cancelLogFetcherJob(session);
        synchronized (lock) {
            sessions.remove(session);
        }
    }

    private ClusterData validateAndGetRequestData(WebSocketSession session, TextMessage message) {
        if (!validMessage(message,session)) {
            return null;
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(message.getPayload(),ClusterData.class);

        } catch (JsonParseException e) {
            LOGGER.error("Error in parsing json",e);
            sendErrorResponse(session, e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Error in mapping incoming message {} to clusterdata object" ,message.toString(),e);
            sendErrorResponse(session, e.getMessage());
        }

        return null;
    }

    private boolean validMessage( TextMessage textMessage,  WebSocketSession webSocketSession)
    {
        if(textMessage == null) {
            LOGGER.error("Request body is null");
            sendErrorResponse(webSocketSession,"Request body is null");
            return false;
        }
        return true;
    }

    private void sendErrorResponse(WebSocketSession session, String error) {
        try {
            if (error != null) {
                session.sendMessage(new TextMessage(error));
            }
        } catch (IOException ioException) {
            LOGGER.error("Error in sending message to ws client {}",error);
        }
    }

    private void handleException(Exception e, WebSocketSession session) {
        try {
            webSocketSessionManager.cancelLogFetcherJob(session);
            session.close(new CloseStatus(CloseStatus.BAD_DATA.getCode(),e.getMessage()));
        } catch (IOException exception) {
            LOGGER.error("Error in closing session",exception);
        }
    }



    protected int getSessionSize(){
        return sessions.size();
    }
}

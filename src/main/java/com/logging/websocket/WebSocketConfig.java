package com.logging.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
class WebSocketConfig implements WebSocketConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketConfig.class);
    private static final String WEBSOCKET_PATH = "/v1/logstream";

    @Bean
    public WebSocketConnectionHandler connectionHandler() {
        return new WebSocketConnectionHandler();
    }
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        LOGGER.info("Registering ws connection handler");
        registry.addHandler(connectionHandler(), WEBSOCKET_PATH).setAllowedOrigins("*") ;
    }
}
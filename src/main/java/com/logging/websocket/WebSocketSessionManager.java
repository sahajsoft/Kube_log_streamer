package com.logging.websocket;

import com.logging.connector.KubeApi;
import com.logging.connector.KubeLogConnector;
import com.logging.connector.KubeLogMessageHandler;
import com.logging.model.ClusterData;
import com.logging.model.PodNamespace;
import io.kubernetes.client.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

@Component
class WebSocketSessionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketSessionManager.class);

    private volatile ConcurrentHashMap<WebSocketSession, List<PodNamespace>> sessionPodMap = new ConcurrentHashMap<>();

    private volatile ConcurrentHashMap<PodNamespace, KubeLogMessageHandler> podSessionsMap = new ConcurrentHashMap<>();

    private int corePoolSize = 15;
    private int maxPoolSize = 20;
    private long keepAliveTime = 5000;

    private ExecutorService threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime,
            TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());

    @Autowired
    private KubeLogConnector kubeLogConnector;

    @Autowired
    KubeApi kubeApi;


    protected Future handleMessage(ClusterData clusterData, WebSocketSession webSocketSession) throws ExecutionException,
            ApiException, InterruptedException, IOException {
        LOGGER.debug("Handling request message");

        return threadPoolExecutor.submit(handleMessageTask(clusterData, webSocketSession));
    }

    private Callable<Void> handleMessageTask(ClusterData clusterData, WebSocketSession webSocketSession) throws IOException,
            ApiException {

        return new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                try {
                    List<String> podList = kubeApi.getPodsByNamespaceLabel(clusterData);
                    LOGGER.debug(podList.toString());
                    if (podList != null && podList.size() > 0) {
                        try {
                            List<Future> futureList = fetchLogsForPods(clusterData, webSocketSession, podList);
                        } catch (Exception e) {
                            LOGGER.error("Caught exception in handle message");
                        }
                    } else {
                        LOGGER.debug("No pods associated with the given cluster info {}", clusterData.toString());
                    }
                } catch (IOException e) {
                    LOGGER.error("Error while fetching logs", e);
                } catch (ApiException e) {
                    LOGGER.error("Error while fetching logs", e);
                }
                return null;
            }
        };

    }

    private List<Future> fetchLogsForPods(ClusterData clusterData, WebSocketSession webSocketSession, List<String> podList) throws InterruptedException, ExecutionException, ApiException {
        List<Future> futureList = new LinkedList<>();
        for (String pod : podList) {
            try {
                //If there is no existing log streaming for pod,create a new one.
                if (!checkLogStreamExists(clusterData, webSocketSession, pod)) {

                    KubeLogMessageHandler logMessageHandler = new KubeLogMessageHandler(null, clusterData.getPod(), clusterData.getNamespace());
                    PodNamespace podNamespace = new PodNamespace(pod, clusterData);

                    Callable callable = startLogStreamTask(podNamespace, webSocketSession, logMessageHandler);

                    Future future = threadPoolExecutor.submit(callable);
                    futureList.add(future);

                    logMessageHandler.setLogTaskFuture(future);
                }
            } catch (ApiException e) {
                LOGGER.error("api exception", e);
            }
        }
        return futureList;

    }

    private boolean checkLogStreamExists(ClusterData clusterData, WebSocketSession webSocketSession, String pod) throws ApiException {

        LOGGER.debug("Current thread is");
        PodNamespace podNamespace = new PodNamespace(pod, clusterData);

        //Check if there is an existing log streaming connection for this pod
        KubeLogMessageHandler logMessageHandler = podSessionsMap.get(podNamespace);

        if (logMessageHandler != null) {
            logMessageHandler.getWebSocketSessions().add(webSocketSession);

            List<PodNamespace> podNamespaceList = sessionPodMap.get(webSocketSession);
            if (podNamespaceList != null) {
                this.sessionPodMap.get(webSocketSession).add(podNamespace);
            } else {
                podNamespaceList = new LinkedList<>();
                podNamespaceList.add(podNamespace);
                sessionPodMap.put(webSocketSession, podNamespaceList);
            }
            return true;
        }
        return false;
    }

    private Callable<Integer> startLogStreamTask(PodNamespace podNamespace, WebSocketSession webSocketSession, KubeLogMessageHandler podSessionHandler) throws ApiException {

        return () -> {
            LOGGER.debug("Invoking new log fetcher job for pod {}", podNamespace.getPod());

            populateSessionDetails(podNamespace, webSocketSession, podSessionHandler);

            try {
                kubeLogConnector.getKubeLogStream(podNamespace, podSessionHandler);
            } catch (ApiException e) {
                LOGGER.error("Error while executing kube log streaming api",e);
//                    webSocketSession.close(new CloseStatus(CloseStatus.BAD_DATA.getCode(), e.getMessage()));
                throw e;
                //cancelLogFetcherJob(webSocketSession);
            }
            return 0;
        };
    }


    private void populateSessionDetails(PodNamespace podNamespace, WebSocketSession webSocketSession, KubeLogMessageHandler podSessionHandler) {
        LOGGER.debug("Adding pod {} to podList", podNamespace.getPod());
        List<PodNamespace> podList = sessionPodMap.get(webSocketSession);

        if (podList != null) {
            LOGGER.debug("adding to existing list");
            sessionPodMap.get(webSocketSession).add(podNamespace);
        } else {
            LOGGER.debug("Creating pod list");
            podList = new LinkedList<>();
            podList.add(podNamespace);
            sessionPodMap.put(webSocketSession, podList);
            LOGGER.debug(" Size is {}", sessionPodMap.size());
        }
        //Add the details to the pod map.
        podSessionsMap.put(podNamespace, podSessionHandler);

        podSessionHandler.getWebSocketSessions().add(webSocketSession);
    }


    private void validateAndStopLogFetcherJob(final WebSocketSession webSocketSession, PodNamespace pod) {
        LOGGER.debug("Validating log fetcher job for pod {}", pod.getPod());
        KubeLogMessageHandler podSessionHandler = podSessionsMap.get(pod);
        if (podSessionHandler != null) {
            List<WebSocketSession> sessionList = podSessionHandler.getWebSocketSessions();
            if (sessionList != null) {
                LOGGER.debug("Removing session {} from sessionlist for pod {}", webSocketSession.toString(), pod.getPod());
                sessionList.remove(webSocketSession);

                //If there are no websocket sessions associated with the pod,stop the scheduled log fetcher job.
                if (sessionList.isEmpty()) {
                    cancelScheduledJob(pod, podSessionHandler);
                }
            }
        }
    }

    private void cancelScheduledJob(PodNamespace pod, KubeLogMessageHandler podSessionHandler) {

        Future future = podSessionHandler.getLogTaskFuture();

        //Cancelling the running job
        if (!future.isCancelled() && !future.isDone()) {
            LOGGER.debug("Cancelling log fetcher job for pod {}", pod.getPod());

            boolean cancelled = future.cancel(true);
            if (cancelled) {
                LOGGER.debug("Cancelled log fetcher job for pod {}", pod.getPod());
            } else {
                LOGGER.warn("Failed to cancel log fetcher job for pod {}", pod.getPod());
            }
        }

        podSessionsMap.remove(pod);
    }

    //Cancel scheduled jobs when there are no active sessions for a pod.
    protected void cancelLogFetcherJob(final WebSocketSession webSocketSession) {
        List<PodNamespace> pods = this.sessionPodMap.get(webSocketSession);
        if (pods != null) {
            for (PodNamespace pod : pods) {
                //Stop the kubelog fetch job and remove the saved session.
                validateAndStopLogFetcherJob(webSocketSession, pod);
            }
        }

        if (this.sessionPodMap.containsKey(webSocketSession)) {
            LOGGER.debug("Removing ws client session {} from sessionpod map", webSocketSession.toString());
            sessionPodMap.remove(webSocketSession);
        }

    }


    protected List<PodNamespace> getPodsForSession(WebSocketSession webSocketSession) {
        LOGGER.debug(" Size isww {}", sessionPodMap.size());
        List<PodNamespace> podNamespaceList = sessionPodMap.get(webSocketSession);
        return podNamespaceList != null ? podNamespaceList : null;
    }

    protected int getNumSessionsForPod(String pod) {
        KubeLogMessageHandler podSessionHandler = podSessionsMap.get(pod);
        if (podSessionHandler != null) {
            return podSessionHandler.getWebSocketSessions() != null ? podSessionHandler.getWebSocketSessions().size() : 0;
        }
        return 0;

    }

    @PreDestroy
    public void closeExecutor() {
        if (threadPoolExecutor != null) {
            LOGGER.debug("Calling executor shutdown");
            threadPoolExecutor.shutdownNow();
            boolean status = threadPoolExecutor.isShutdown();
            LOGGER.debug("Shutdown status is {}", status);
        }
    }


}


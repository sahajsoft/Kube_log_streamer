package com.logging.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

public class PodSessionData{

    private static final Logger LOGGER = LoggerFactory.getLogger(PodSessionData.class);

    private String podName;
    private String namespace;
    private String label;

    private List<WebSocketSession> webSocketSessions = Collections.synchronizedList(new ArrayList<>());

    private ScheduledFuture scheduledFuture;

    private Future logTaskFuture;

    public PodSessionData() {
    }

    public PodSessionData(ScheduledFuture scheduledFuture, String podName, String namespace) {
        this.scheduledFuture = scheduledFuture;
        this.podName = podName;
        this.namespace = namespace;
    }

    public List<WebSocketSession> getWebSocketSessions() {
        return webSocketSessions;
    }

    public void setWebSocketSessions(List<WebSocketSession> webSocketSessions) {
        this.webSocketSessions = webSocketSessions;
    }


    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setScheduledFuture(ScheduledFuture scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
    }

    public ScheduledFuture getScheduledFuture() {
        return scheduledFuture;
    }

    public void setLogTaskFuture(Future logTaskFuture) {
        this.logTaskFuture = logTaskFuture;
    }

    public Future getLogTaskFuture() {
        return logTaskFuture;
    }

    public static final class PodSessionDataBuilder {
        private String podName;
        private String namespace;
        private String label;
        private List<WebSocketSession> webSocketSessions = Collections.synchronizedList(new ArrayList<>());
        private ScheduledFuture scheduledFuture;
        private Future logTaskFuture;

        private PodSessionDataBuilder() {
        }

        public static PodSessionDataBuilder aPodSessionData() {
            return new PodSessionDataBuilder();
        }

        public PodSessionDataBuilder withPodName(String podName) {
            this.podName = podName;
            return this;
        }

        public PodSessionDataBuilder withNamespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public PodSessionDataBuilder withLabel(String label) {
            this.label = label;
            return this;
        }

        public PodSessionDataBuilder withWebSocketSessions(List<WebSocketSession> webSocketSessions) {
            this.webSocketSessions = webSocketSessions;
            return this;
        }

        public PodSessionDataBuilder withScheduledFuture(ScheduledFuture scheduledFuture) {
            this.scheduledFuture = scheduledFuture;
            return this;
        }

        public PodSessionDataBuilder withLogTaskFuture(Future logTaskFuture) {
            this.logTaskFuture = logTaskFuture;
            return this;
        }

        public PodSessionData build() {
            PodSessionData podSessionData = new PodSessionData();
            podSessionData.setPodName(podName);
            podSessionData.setNamespace(namespace);
            podSessionData.setLabel(label);
            podSessionData.setWebSocketSessions(webSocketSessions);
            podSessionData.setScheduledFuture(scheduledFuture);
            podSessionData.setLogTaskFuture(logTaskFuture);
            return podSessionData;
        }
    }
}

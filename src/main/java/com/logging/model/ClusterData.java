package com.logging.model;

import java.util.Objects;

public class ClusterData {

    private static final String DEFAULT_FIELD_SELECTOR = "";

    private String namespace;

    private String pod;

    private String container;

    private String label;

    private String fieldSelector = DEFAULT_FIELD_SELECTOR;

    private int sinceSeconds = 30;

    private int numLogLines = 10;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public int getNumLogLines() {
        return numLogLines;
    }

    public void setNumLogLines(int numLogLines) {
        this.numLogLines = numLogLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClusterData that = (ClusterData) o;
        return Objects.equals(namespace, that.namespace) && Objects.equals(container, that.container) &&
                Objects.equals(pod, that.pod);
    }

    @Override
    public int hashCode() {

        return Objects.hash(namespace, pod);
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public int getSinceSeconds() {
        return sinceSeconds;
    }

    public void setSinceSeconds(int sinceSeconds) {
        this.sinceSeconds = sinceSeconds;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFieldSelector() {
        return fieldSelector;
    }

    public void setFieldSelector(String fieldSelector) {
        this.fieldSelector = fieldSelector;
    }

    @Override
    public String toString() {
        return "ClusterData{" +
                "namespace='" + namespace + '\'' +
                ", pod='" + pod + '\'' +
                ", container='" + container + '\'' +
                ", label='" + label + '\'' +
                ", fieldSelector='" + fieldSelector + '\'' +
                ", sinceSeconds=" + sinceSeconds +
                ", numLogLines=" + numLogLines +
                '}';
    }

    public static final class ClusterDataBuilder {
        private String namespace;
        private String pod;
        private int numLogLines;

        private ClusterDataBuilder() {
        }

        public static ClusterDataBuilder aClusterData() {
            return new ClusterDataBuilder();
        }

        public ClusterDataBuilder withNamespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public ClusterDataBuilder withPod(String pod) {
            this.pod = pod;
            return this;
        }

        public ClusterDataBuilder withNumLogLines(int numLogLines) {
            this.numLogLines = numLogLines;
            return this;
        }

        public ClusterData build() {
            ClusterData clusterData = new ClusterData();
            clusterData.setNamespace(namespace);
            clusterData.setPod(pod);
            clusterData.setNumLogLines(numLogLines);
            return clusterData;
        }
    }
}

package com.logging.model;

import java.util.Objects;

public class PodNamespace {

    private String pod;

    private static final String DEFAULT_FIELD_SELECTOR = "";

    private String namespace;

    private String container;

    private String label;

    private String fieldSelector = DEFAULT_FIELD_SELECTOR;

    private int sinceSeconds = 30;

    private int numLogLines = 10;

    public PodNamespace(String pod, String namespace) {
        this.pod = pod;
        this.namespace = namespace;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFieldSelector() {
        return fieldSelector;
    }

    public void setFieldSelector(String fieldSelector) {
        this.fieldSelector = fieldSelector;
    }

    public int getSinceSeconds() {
        return sinceSeconds;
    }

    public void setSinceSeconds(int sinceSeconds) {
        this.sinceSeconds = sinceSeconds;
    }

    public int getNumLogLines() {
        return numLogLines;
    }

    public void setNumLogLines(int numLogLines) {
        this.numLogLines = numLogLines;
    }

    public PodNamespace(String podName,ClusterData clusterData) {
        this.namespace = clusterData.getNamespace();
        this.container = clusterData.getContainer();
        this.fieldSelector = clusterData.getFieldSelector();
        this.pod = podName;
        this.sinceSeconds = clusterData.getSinceSeconds();
        this.label = clusterData.getLabel();
        this.numLogLines = clusterData.getNumLogLines();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PodNamespace that = (PodNamespace) o;
        return Objects.equals(pod, that.pod) &&
                Objects.equals(namespace, that.namespace) &&
                Objects.equals(container, that.container);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pod, namespace, container);
    }
}

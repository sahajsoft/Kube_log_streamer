package com.logging.connector;

import io.kubernetes.client.ApiClient;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;
import io.kubernetes.client.util.authenticators.GCPAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KubeApiUtil {

    @Value("${kube.master.hostname}")
    private String kubeMasterHostName;

    private static final Logger LOGGER = LoggerFactory.getLogger(KubeApiUtil.class);

    public CoreV1Api getk8sCoreApi() throws IOException {
        LOGGER.debug("Creating kube core api client");
        return new CoreV1Api(setupK8sApiClient(kubeMasterHostName));
    }

    private ApiClient setupK8sApiClient(String kubeMasterHostName) throws IOException {
        KubeConfig.registerAuthenticator(new GCPAuthenticator());
        ApiClient client = ClientBuilder
                .standard()
                .setBasePath(kubeMasterHostName)
                .setVerifyingSsl(false)
                .build();
        Configuration.setDefaultApiClient(client);
        return client;
    }
}

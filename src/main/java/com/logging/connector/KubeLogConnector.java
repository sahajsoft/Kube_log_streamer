package com.logging.connector;

import com.logging.CallbackHandler;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.PodLogs;
import io.kubernetes.client.apis.CoreV1Api;
import com.logging.model.PodNamespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;


@Component
@PropertySource("classpath:application.yml")
public class KubeLogConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger(KubeLogConnector.class);

    private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;
    private static final int KUBE_CLIENT_READ_TIMEOUT = 0;

    @Autowired
    private KubeApiUtil kubeApiUtil;


    public KubeLogConnector() {
    }

    /**
     * @param podNamespace
     * @param callbackHandler
     */
    public void getKubeLogStream(final PodNamespace podNamespace, CallbackHandler callbackHandler) throws ApiException {

        try {
            CoreV1Api coreV1Api = kubeApiUtil.getk8sCoreApi();

            //Set read timeout to 0 to prevent socket connection from timing out.
            coreV1Api.getApiClient().getHttpClient().setReadTimeout(0, TimeUnit.SECONDS);

            doLogApiCall(podNamespace, callbackHandler, coreV1Api);
        } catch (IOException e) {
            LOGGER.error("Error in getting K8s CoreV1Api", e);
        }

    }

    private void doLogApiCall(final PodNamespace podNamespace, CallbackHandler callbackHandler, CoreV1Api coreV1Api) throws ApiException {
        LOGGER.debug("Setting connection parameters for kubernetes client socket connection");
        //Set read timeout to 0 to prevent socket connection from timing out.
        coreV1Api.getApiClient().getHttpClient().setReadTimeout(KUBE_CLIENT_READ_TIMEOUT, TimeUnit.SECONDS);

        readStreamLogs(podNamespace, callbackHandler);
    }

    private void readStreamLogs(final PodNamespace podNamespace, CallbackHandler callbackHandler) throws ApiException {
        PodLogs podLogs = new PodLogs();
        try (InputStream inputStream = podLogs.streamNamespacedPodLog(podNamespace.getNamespace(), podNamespace.getPod(),
                podNamespace.getContainer(), podNamespace.getSinceSeconds(), podNamespace.getNumLogLines(), false)) {
            LOGGER.debug("before buffereader");

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, DEFAULT_ENCODING))) {

                String logLine;

                //Run until the thread is interrupted. Thread is interrupted if there are no ws client sessions for the pod
                while (!Thread.currentThread().isInterrupted()) {
                    if ((logLine = bufferedReader.readLine()) != null) {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(podNamespace.getPod()).append(" ").append(logLine);

                        callbackHandler.onMessage(stringBuilder.toString());
                    }
                }
            }
        } catch (IOException e) {
            if (!(e instanceof InterruptedIOException)) {
                LOGGER.error("Error while invoking api streamNamespacedPodLog", e);
            }
        }
        finally {
            callbackHandler.onClose();
        }
    }

}

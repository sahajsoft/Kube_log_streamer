package com.logging.connector;

import com.logging.CallbackHandler;
import com.logging.model.PodSessionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

public class KubeLogMessageHandler implements CallbackHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(KubeLogMessageHandler.class);

    private PodSessionData podSessionData = new PodSessionData();

    private String podName;
    private String namespace;
    private String label;

    private List<WebSocketSession> webSocketSessions = Collections.synchronizedList(new ArrayList<>());

    private ScheduledFuture scheduledFuture;

    private Future logTaskFuture;

    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    public KubeLogMessageHandler() {
    }

    public KubeLogMessageHandler(ScheduledFuture scheduledFuture, String podName, String namespace) {
        this.scheduledFuture = scheduledFuture;
        this.podName = podName;
        this.namespace = namespace;
    }

    public List<WebSocketSession> getWebSocketSessions() {
        return webSocketSessions;
    }

    public void setWebSocketSessions(List<WebSocketSession> webSocketSessions) {
        this.webSocketSessions = webSocketSessions;
    }

    public KubeLogMessageHandler(Vector<WebSocketSession> socketSessions) {
        this.webSocketSessions = socketSessions;
    }

    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setScheduledFuture(ScheduledFuture scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
    }

    public ScheduledFuture getScheduledFuture() {
        return scheduledFuture;
    }

    public void setLogTaskFuture(Future logTaskFuture) {
        this.logTaskFuture = logTaskFuture;
    }

    public Future getLogTaskFuture() {
        return logTaskFuture;
    }

    public void onMessage(String message) {
        if (message != null) {

            //Send loglines to websocket clients.
            synchronized (webSocketSessions) {
                Iterator iterator = webSocketSessions.iterator();
                while (iterator.hasNext()) {
                    WebSocketSession webSocketSession = (WebSocketSession) iterator.next();
                    //LOGGER.debug("Sending message to ws client session {}", webSocketSession.getRemoteAddress());
                    executorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                webSocketSession.sendMessage(new TextMessage(message));
                            } catch (IOException e) {
                                LOGGER.error("Error in sending message {} to ws client {}", message,
                                        webSocketSession.getRemoteAddress());
                            }
                        }
                    });
                }
            }

        }
    }

    @PreDestroy
    public void closeExecutor() {
        if (!executorService.isTerminated()) {
            LOGGER.debug("Calling executor shutdown");
            executorService.shutdownNow();
            LOGGER.debug("Shutdown status is {}", executorService.isShutdown());
        }
    }

    public void onClose() {
        if (!executorService.isTerminated()) {
            LOGGER.debug("Calling executor shutdown");
            executorService.shutdownNow();
            LOGGER.debug("Shutdown status is {}", executorService.isShutdown());
        }
    }

}

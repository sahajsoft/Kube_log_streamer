package com.logging.connector;

import com.logging.model.ClusterData;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class KubeApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(KubeApi.class);

    @Autowired
    private KubeApiUtil kubeApiUtil;

    public List<String> getPodsByNamespaceLabel(final ClusterData clusterData) throws IOException, ApiException {
        try {
            CoreV1Api coreV1Api = kubeApiUtil.getk8sCoreApi();
            LOGGER.debug("Calling listNamespacedPod api");
            V1PodList v1PodList = coreV1Api.listNamespacedPod(clusterData.getNamespace(),null,null,clusterData.getFieldSelector(),null,clusterData.getLabel(),null,null,null,null);
            if(v1PodList != null) {
                List<V1Pod> podList =  v1PodList.getItems();
                if (podList != null) {
                    LOGGER.debug("Found pods associated with ");
                    return podList.stream().filter(pod -> pod.getMetadata() != null && !StringUtils.isBlank(pod.getMetadata().getName())).map(pod -> pod.getMetadata().getName()).collect(Collectors.toCollection(LinkedList::new));
                }
            }
        } catch (IOException | ApiException e) {
            LOGGER.error("Error while executing listNamespacedPod api ",e);
            throw  e;
        }
        return null;

    }
}

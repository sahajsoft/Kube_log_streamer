package com.logging;

public interface CallbackHandler {
    default void onMessage(String message) {
    }

    default void onClose(){}
}
